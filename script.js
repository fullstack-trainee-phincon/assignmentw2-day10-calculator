let toggleSwitch = document.getElementsByClassName("redButton")[0];
let delButton = document.getElementById("del");
let equalButton = document.getElementById("equal");
let textCalc = document.getElementById("text-calc");
let textThemes = document.getElementById("text-themes");
let result = document.getElementById("result");

function go_to_1() {
  toggleSwitch.classList.add("horizTranslate1");
  toggleSwitch.classList.remove("horizTranslate2");
  toggleSwitch.classList.remove("horizTranslate3");
  document.getElementById("container").style.backgroundColor =
    "rgb(0, 73, 107)";
  delButton.style.backgroundColor = "rgb(105, 70, 192)";
  document.getElementById("calculator").style.backgroundColor = "#222D41";
  document.getElementById("display").style.backgroundColor = "#222D41";
  document.document.getElementById("legendTextContainer").style.color =
    "#ffffff";
  equalButton.style.backgroundColor = "rgb(255, 68, 68)";
  textCalc.style.color = "#ffffff";
  textThemes.style.color = "#ffffff";
  result.style.color = "#ffffff";
  doStuff(1);
}

function go_to_2() {
  toggleSwitch.classList.add("horizTranslate2");
  toggleSwitch.classList.remove("horizTranslate3");
  toggleSwitch.classList.remove("horizTranslate1");
  document.getElementById("container").style.backgroundColor = "#E5E5E5";
  document.getElementById("calculator").style.backgroundColor = "#D3CCCA";
  document.getElementById("display").style.backgroundColor = "#ffffff";
  document.getElementById("legendTextContainer").style.color = "#222222";
  delButton.style.backgroundColor = "#00a295";
  equalButton.style.backgroundColor = "#00a295";
  textCalc.style.color = "#000000";
  textThemes.style.color = "#000000";
  result.style.color = "#000000";
  doStuff(2);
}

function go_to_3() {
  toggleSwitch.classList.add("horizTranslate3");
  toggleSwitch.classList.remove("horizTranslate2");
  toggleSwitch.classList.remove("horizTranslate1");
  document.getElementById("container").style.backgroundColor = "#292247";
  document.getElementById("buttonContainer").style.backgroundColor = "#444444";
  document.getElementById("legendTextContainer").style.color = "#E2D241";
  document.getElementById("display").style.backgroundColor = "#0e162f";
  document.getElementById("calculator").style.backgroundColor = "#1c2643";
  delButton.style.backgroundColor = "#9d80fd";
  equalButton.style.backgroundColor = "#ee3939";
  textCalc.style.color = "#ffffff";
  textThemes.style.color = "#ffffff";
  result.style.color = "#f0ff4";
  doStuff(3);
}

function appendToResult(value) {
  const result = document.getElementById("result");
  result.value += value;
}

function calculateResult() {
  const result = document.getElementById("result");
  try {
    result.value = eval(result.value);
  } catch (error) {
    result.value = "Error";
  }
}

function clearResult() {
  const result = document.getElementById("result");
  result.value = "";
}

function deleteNumber() {
  const result = document.getElementById("result");
  result.value = result.value.slice(0, -1);
}
